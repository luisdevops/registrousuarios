package com.example.demo.user;

import com.example.demo.model.Config;
import com.example.demo.repository.ConfigRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.model.User;
import com.example.demo.util.UtilsTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@WebAppConfiguration
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserIntegrationTest {

	protected MockMvc mockMvc;
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ConfigRepository configRepository;

	@Autowired
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@BeforeAll
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
	}

	protected String json(Object object) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(object, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

	@Test
	public void shouldReturnErrorRegisterEmailExistTest() throws Exception {
		userRepository.deleteAll();

		shouldReturnSuccessRegisterUserTest();
		mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json(UtilsTest.getRegister())))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.mensaje").value("error, email ya existe."));
	}

	@Test
	public void shouldReturnSuccessRegisterUserTest() throws Exception {
		userRepository.deleteAll();
		User user = UtilsTest.getRegister();
		mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json(user)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.id").isNotEmpty())
				.andExpect(jsonPath("$.created").isNotEmpty())
				.andExpect(jsonPath("$.modified").isNotEmpty())
				.andExpect(jsonPath("$.last_login").isNotEmpty())
				.andExpect(jsonPath("$.token").isNotEmpty())
				.andExpect(jsonPath("$.isactive").isBoolean());
	}


	@Test
	public void registerTestErrorEmailRegularExpression() throws Exception {
		User user = UtilsTest.getRegister();
		user.setEmail("juand@");
		mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json(user)))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.mensaje").value("Formato Incorrecto del correo."));
	}



	@Test
	public void registerTestErrorPassRegularExpression() throws Exception {
		userRepository.deleteAll();

		List<Config> configs = new ArrayList<>();
		configs.add(new Config("001", "(?=.*?[A-Z])", "", true ));
		configs.add(new Config("002", "(?=.*?[a-z])", "", true ));
		configs.add(new Config("003", "(?=.*?[0-9])", "", true ));
		configs.add(new Config("004", "(?=.*?[#?!@$%^&*-])", "", true ));
		configs.add(new Config("005", ".{8,}", "", true ));

		configRepository.saveAll(configs);

		User user = UtilsTest.getRegister();
		user.setPassword("juand@");
		mockMvc.perform(post("/user")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(json(user)))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(jsonPath("$.mensaje").value("Formato Incorrecto del pass"));
	}


}
