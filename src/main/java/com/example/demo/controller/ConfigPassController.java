package com.example.demo.controller;

import com.example.demo.model.Config;
import com.example.demo.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("config-pass")
public class ConfigPassController {

	@Autowired
	private ConfigService configService;

	@GetMapping
	public List<Config> configList(){
		return this.configService.getAllCongig();
	}

	@PostMapping
	public List<Config> saveConfig(@RequestBody List<Config> configs){
		return this.configService.saveConfig(configs);
	}


}
