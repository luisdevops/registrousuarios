package com.example.demo.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.exception.ModelBadRequestException;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public final class Utils {

	public static final String EMAIL_EXIST = "error, email ya existe.";
	public static final String EMAIL_NOT_FORMAT = "Formato Incorrecto del correo.";
	public static final String PASS_NOT_FORMAT = "Formato Incorrecto del pass";
	public static final String PASS_NOT_CORRECT = "Pass incorrecto";
	public static final String INIT_REGEX = "^";
	public static final String END_REGEX = "$";



	private static final String ISSUER = "auth0";
	private static final String SECRET = "S3cretPass";
	private static final Integer EXPIRES_DAY = 5;

	private static String REGEX_EMAIL = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

	public static void validateEmail(String email){
		if(!Pattern.compile(REGEX_EMAIL).matcher(email).matches()){
			throw new ModelBadRequestException(EMAIL_NOT_FORMAT);
		}
	}


	public static void validatePassword(String pass, String regex){
		if(!regex.equals("") && !Pattern.compile(INIT_REGEX+regex+END_REGEX).matcher(pass).matches()){
			throw new ModelBadRequestException(PASS_NOT_FORMAT);
		}
	}


	public static String createToken(String data) {
		return JWT.create()
				.withIssuer(ISSUER)
				.withExpiresAt(expire(EXPIRES_DAY))
				.withClaim(ISSUER, data)
				.sign(Algorithm.HMAC512(SECRET));
	}

	public String validateToken(String token) {
		JWTVerifier jwtVerifier = JWT
				.require(Algorithm.HMAC512(SECRET))
				.withIssuer(ISSUER)
				.build();
		DecodedJWT decodedJWT = jwtVerifier.verify(cleanToken(token));
		String data = decodedJWT.getClaim(ISSUER).asString();
		return String.valueOf(data);
	}

	private static Date expire(int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, days);
		return calendar.getTime();
	}

	private static String cleanToken(String token) {
		String aux = token;
		if (aux != null && aux.contains("Bearer")) {
			aux = aux.replace("Bearer ", "");
			aux = aux.trim();
		}
		return aux;
	}

}
