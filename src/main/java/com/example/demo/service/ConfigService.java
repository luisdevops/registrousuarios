package com.example.demo.service;

import com.example.demo.model.Config;

import java.util.List;

public interface ConfigService {

	List<Config> getAllCongig();

	List<Config>  saveConfig(List<Config> config);

}
