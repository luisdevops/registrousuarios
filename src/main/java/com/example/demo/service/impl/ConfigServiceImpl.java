package com.example.demo.service.impl;

import com.example.demo.repository.ConfigRepository;
import com.example.demo.model.Config;
import com.example.demo.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfigServiceImpl implements ConfigService {

	@Autowired
	private ConfigRepository configDao;

	@Override
	public List<Config> getAllCongig() {
		return this.configDao.findAll();
	}

	@Override
	public List<Config> saveConfig(List<Config> config) {
		return this.configDao.saveAll(config);
	}
}
