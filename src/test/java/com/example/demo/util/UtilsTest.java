package com.example.demo.util;

import com.example.demo.model.Phone;
import com.example.demo.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UtilsTest {

	public static User getRegister() {
		User registerDto = new User();
		registerDto.setName("Jhon Doe");
		registerDto.setEmail("juan@rodriguez.org");
		registerDto.setPassword("aaBB12.%&%");
		registerDto.setPhones(getPhones());
		return registerDto;
	}

	public static List<Phone> getPhones() {
		List<Phone> phoneDtos = new ArrayList<>();
		Phone phone = new Phone();
		phone.setNumber("123456789");
		phone.setCitycode("1");
		phone.setContrycode("51");
		phoneDtos.add(phone);
		return phoneDtos;
	}
}
