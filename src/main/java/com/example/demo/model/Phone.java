package com.example.demo.model;

import com.example.demo.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "phone")
public class Phone {

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = true, foreignKey = @ForeignKey(name = "id_phone_user"))
	private User userId;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPhone;

	@Column
	private String number;

	@Column
	private String citycode;

	@Column
	private String contrycode;


	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getContrycode() {
		return contrycode;
	}

	public void setContrycode(String contrycode) {
		this.contrycode = contrycode;
	}

	public Integer getIdPhone() {
		return idPhone;
	}

	public void setIdPhone(Integer idPhone) {
		this.idPhone = idPhone;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	public Phone(String number, String citycode, String contrycode) {
		this.number = number;
		this.citycode = citycode;
		this.contrycode = contrycode;
	}

	public Phone(){

	}

	public static List<Phone> getPhones(UserDto userDto) {
		List<Phone> phones = new ArrayList<>();
		userDto.getPhones().forEach(phoneDto -> phones.add( new Phone(phoneDto.getNumber(), phoneDto.getCitycode(), phoneDto.getContrycode()) ));
		return phones;
	}

}
