package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "config")
public class Config {

	@Id
	private String idConfig;
	private String value;
	private String description;
	private boolean isActive;

	public String getIdConfig() {
		return idConfig;
	}

	public void setIdConfig(String idConfig) {
		this.idConfig = idConfig;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean active) {
		isActive = active;
	}

	public Config(String idConfig, String value, String description, boolean isActive) {
		this.idConfig = idConfig;
		this.value = value;
		this.description = description;
		this.isActive = isActive;
	}

	public Config(){
	}

}
