package com.example.demo.util;

import com.example.demo.exception.ModelBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class Crypto {

	@Autowired
	private PasswordEncoder passwordEncoder;

	public String encode(String text) {
		return passwordEncoder.encode(text);
	}

	public void checkPassword(String text, String encodedText) throws ModelBadRequestException {
		if (!validate(text, encodedText)) {
			throw new ModelBadRequestException(Utils.PASS_NOT_CORRECT);
		}
	}

	public boolean validate(String text, String encodedText) {
		return passwordEncoder.matches(text, encodedText);
	}
}
