package com.example.demo.service.impl;

import com.example.demo.exception.ModelUnprocesaableEntityException;
import com.example.demo.repository.UserRepository;
import com.example.demo.dto.UserDto;
import com.example.demo.exception.ModelBadRequestException;
import com.example.demo.model.Config;
import com.example.demo.model.Phone;
import com.example.demo.model.User;
import com.example.demo.service.ConfigService;
import com.example.demo.service.UsearService;
import com.example.demo.util.Crypto;
import com.example.demo.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UsearService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private Crypto crypto;

	@Override
	public User register(UserDto userDto) {

		Utils.validatePassword(userDto.getPassword(), getAllConfig());
		Utils.validateEmail(userDto.getEmail());

		validateEmail(userDto.getEmail());
		User user = new User(userDto.getName(), userDto.getEmail(), userDto.getPassword(), Phone.getPhones(userDto));
		user.setToken(Utils.createToken(user.getName()));
		user.setIsactive(true);
		user.setPassword(crypto.encode(user.getPassword()));
		user.getPhones().forEach(det -> det.setUserId(user));
		return this.userRepository.save(user);
	}


	private String getAllConfig(){
		List<Config> configs =  this.configService.getAllCongig();
		return configs.stream().filter(config -> config.isActive())
				.map(Config::getValue)
		.collect(Collectors.joining(""));

	}

	private void validateEmail(String email){
		if(this.userRepository.findUserForEmail(email) != null){
			throw new ModelUnprocesaableEntityException(Utils.EMAIL_EXIST);
		}

	}

}
